LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY Q1 IS
PORT (D,R,CLK :   IN      std_logic;
        Q               :   OUT std_logic
        );
END ENTITY Q1;

ARCHITECTURE behavioural OF Q1 IS
BEGIN
D_FF : PROCESS (CLK, R)
BEGIN
  IF rising_edge(CLK) THEN
    Q <= D;
  END IF;
  IF R = '0' THEN
    Q <= '0';
  END IF;
END PROCESS D_FF;
END ARCHITECTURE behavioural;
