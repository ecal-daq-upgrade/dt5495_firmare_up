-- ========================================================================
-- ****************************************************************************
-- Company:         CAEN SpA - Viareggio - Italy
-- Model:           V2495 -  Multipurpose Programmable Trigger Unit
-- FPGA Proj. Name: 
-- Device:          ALTERA 5CGXBC4C6F27C7N
-- Author:          
-- Date:            
-- ----------------------------------------------------------------------------
-- Module:          V2495_GateDelay
-- Description:     Top design
-- ****************************************************************************

-- ############################################################################
-- Revision History:
-- ############################################################################

library ieee;
library pll;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use work.V2495_pkg.all;
--use work.fliflopd.all;

entity V2495_GateDelay is

    port
    (
        -- --------------------------------------------------
        -- CLOCK
        CLK        : in    std_logic;                        

      -- --------------------------------------------------------
    -- Mainboard I/O ports
    -- --------------------------------------------------------   
      -- Port A : 32-bit LVDS/ECL input
         A        : in    std_logic_vector (31 DOWNTO 0);    -- Data bus 
      -- Port B : 32-bit LVDS/ECL input                      
         B        : in    std_logic_vector (31 DOWNTO 0);    -- Data bus
      -- Port C : 32-bit LVDS output                         
         C        : out   std_logic_vector (31 DOWNTO 0);    -- Data bus
      -- Port G : 2 NIM/TTL input/output                     
         GIN      : in    std_logic_vector ( 1 DOWNTO 0);    -- In data
         GOUT     : out   std_logic_vector ( 1 DOWNTO 0);    -- Out data
         SELG     : out   std_logic;                         -- Level select
         nOEG     : out   std_logic;                         -- Output Enable
      
         -- --------------------------------------------------
         -- LOCAL BUS
         nREADY       : out   std_logic;                       --! Local Bus slave ready
         nINT         : out   std_logic;                       --! Richiesta di interruzione dal slave
         nBLAST       : in    std_logic;                       --! Segnala l'ultimo ciclo di un trasferimento
         WnR          : in    std_logic;                       --! Selettore di lettura/scrittura
         nADS         : in    std_logic;                       --! Address strobe
         LAD          : inout std_logic_vector (15 DOWNTO 0);  --! Bus Dati/Indirizzi
         nLBRES       : in    std_logic;
         
      -- --------------------------------------------------------
    -- Expansion slots
    -- --------------------------------------------------------                                                                  
      -- PORT D Expansion control signals                    
         IDD      : in    std_logic_vector ( 2 DOWNTO 0);    -- Card ID
         SELD     : out   std_logic;                         -- Level select
         nOED     : out   std_logic;                         -- Output Enable
--         nOED     : in   std_logic;
         D        : inout std_logic_vector (31 DOWNTO 0);    -- Data bus
                                                             
      -- PORT E Expansion control signals                    
         IDE      : in    std_logic_vector ( 2 DOWNTO 0);    -- Card ID
         SELE     : out   std_logic;                         -- Level select
         nOEE     : out   std_logic;                         -- Output Enable
--         nOEE     : in   std_logic;
         E        : inout std_logic_vector (31 DOWNTO 0);    -- Data bus
                                                             
      -- PORT F Expansion control signals                    
         IDF      : in    std_logic_vector ( 2 DOWNTO 0);    -- Card ID
         SELF     : out   std_logic;                         -- Level select
         nOEF     : out   std_logic;                         -- Output Enable
         F        : inout std_logic_vector (31 DOWNTO 0);    -- Data bus
        
        -- -------------------------------------------------
        -- Gate & Delay
        gd_start     : out std_logic_vector(31 downto 0);     -- HACK port name to be reviewed
        gd_delayed   : in  std_logic_vector(31 downto 0);     -- HACK port name to be reviewed
        
        -- -------------------------------------------------
        -- SPI        
        spi_sclk     : out   std_logic;                     
        spi_cs       : out   std_logic;                    
        spi_mosi     : out   std_logic;                     
        spi_miso     : in    std_logic;

        -- --------------------------------------------------------
    -- LED
    -- --------------------------------------------------------
    LED : out std_logic_vector(7 downto 0)                  -- User led                                                  
    );
end V2495_GateDelay;

architecture rtl of V2495_GateDelay is

  signal flipflopsignal : std_logic;
  signal flipflopout: std_logic;


  signal reset      : std_logic;
  signal mon_regs   : MONITOR_REGS_T;
  signal ctrl_regs  : CONTROL_REGS_T;
  signal data_a_reg : DATA_REGS_A_T;
  signal data_b_reg : DATA_REGS_B_T;

  --GATE AND DELAY
  signal gd_data_wr   :  std_logic_vector(31 downto 0);
  signal gd_data_rd   :  std_logic_vector(31 downto 0);
  signal gd_command   :  std_logic_vector(15 downto 0);
  signal gd_write     :  std_logic;
  signal gd_read      :  std_logic;
  signal gd_ready     :  std_logic :='1';


  signal counterL  : Unsigned (31 downto 0) := (others => '0'); 


  --START SIGNAL FOR DELAY LINE    
  signal start_select   : std_logic;
  signal common_start   : std_logic;
  signal counter_rate   : std_logic_vector(31 downto 0):=x"0000F000";
  signal counter_gate   : std_logic_vector(31 downto 0):=x"00008000";
  signal rate           : std_logic_vector(31 downto 0):=x"0000000C";
  
  ------------------------
  --Signals added for trigger firmware

  -- Input Port Enable parameter
  signal inputs             : std_logic_vector(7 downto 0);                  -- All input ports (From port E)
  signal flipped_input      : std_logic_vector(7 downto 0);                  -- Selection of inputs to have their signals flipped
  signal enable_selection   : std_logic_vector(7 downto 0);                  -- for the or gates, 1 to enable channel, 0 to disable
  signal enable_flag        : std_logic_vector(7 downto 0);                  -- final selection of enabled inputs (does not include control port)
  signal enabled_signals    : std_logic_vector(7 downto 0);                  -- input signals of final selection
  signal combined_enabled   : std_logic;                                     -- logical AND between all enabled input signals
  
  -- Control Signal Parameters
  --signal control_signal_selection   : std_logic_vector(3 downto 0);  -- Input from ctrl register to select which input port will supply the control signal
                                                                     -- Using only 4 bits to ensure that only one signal can be chosen as the control signal
  
  --signal control_signal             : std_logic;                     -- Control Signal - untampered
  signal control_delayed            : std_logic;                     -- Final Delayed Control Signal
--  signal full_delay_time            : std_logic_vector(31 downto 0); -- Input from ctrl register to specify the full delay time

  --Self-triggering rate and control
  signal st_downtime                : integer;
  signal st_uptime                  : integer;
  signal st_enable                  : std_logic;
 
  signal st_enable_vec              : std_logic; 
  signal st_downtime_vec            : std_logic_vector(31 downto 0);
  signal st_uptime_vec              : std_logic_vector(31 downto 0);

  signal st_counter                 : integer := 0;

  -- Laser
  signal laser_enable               : std_logic;

  -- External Clock
  signal pll_clk                    : std_logic;
  signal clk_usr                    : std_logic;

  -- Final Output Signal Parameters (The Final Trigger)
  signal final_signal           : std_logic;
  signal final_signal_delayed   : std_logic_vector(9 downto 0);

  -- Busy Flags 
  signal busy_flags           : std_logic_vector(7 downto 0) := (others => '0'); -- Contains all busy flags. '1' for BUSY, '0' for NOT BUSY
  signal enabled_busy_flags   : std_logic_vector(7 downto 0) := (others => '0'); -- List of all busy flags that are being used
  signal busy_flags_delayed   : std_logic_vector(7 downto 0);                    -- All busy flags delayed by user specified amount of time
  signal combined_busy        : std_logic := '0';                                -- logical OR between all input busy flags 
  signal sps_busy_flags			: std_logic := '0';  -- Busy flags for SPS signals from RC
  signal combined_busy_vector : std_logic_vector(7 downto 0) := (others => '0');
  signal busy_edge_detect_first : std_logic_vector(7 downto 0) := (others => '0');
  signal busy_edge_detect_sec   : std_logic_vector(7 downto 0) := (others => '0');
  signal rst_busy_edge_detect    : std_logic_vector(1 downto 0) := (others => '0');
  signal internal_busy_reset     : std_logic := '0';  -- Signal following reset from ctrl_regs(14) controlled by RC

  signal sps_signals          : std_logic_vector(2 downto 0) := (others => '0'); -- All of the sps signals will be located here  
  signal ee_edge_detect			: std_logic_vector(1 downto 0) := (others => '0');      -- Used to detect rising edge of ee
  signal we_edge_detect			: std_logic_vector(1 downto 0) := (others => '0');	-- Used to detect rising edge of we
  signal wwe_edge_detect		: std_logic_vector(1 downto 0) := (others => '0');	-- Used to detect rising edge of wwe
 
  signal trigger_edge_detect            : std_logic_vector(1 downto 0) := (others => '0');      -- Used to detect rising edge of trigger (or final_signal)

  signal sps_counter                    : integer := 0;
  signal trigger_flag_counter           : integer := 0;
  signal sps_sim_enable                 : std_logic := '0';
  signal internal_busy                  : std_logic := '0';
  signal busy_counter                   : integer := 0;
  signal strobe_signal                  : std_logic := '0';                                       -- connected to reg 0x180C to strobe fast commands to BCP
  signal strobe_edge_detect             : std_logic_vector(1 downto 0) := (others => '0');      -- Used to detect rising edge of strobe_signal

--  signal internal_busy_vec              : std_logic_vector(15 downto 0);
--  signal internal_busy_length           : integer range 0 to 65535;

  -- Replacement of the CAEN GDG with our own version so we can run at 40 MHz instead
  -- of using the board clock at 50 MHz
  -- shift_reg_array_t is an array of "shift registers" used to delay and gate the signals 
  type shift_reg_array_t is array (0 to 15) of std_logic_vector(1023 downto 0);
  signal shift_reg_array_r : shift_reg_array_t := (others => (others => '0'));

  type delay_pointer_array_t is array (0 to 15) of std_logic_vector(31 downto 0);
  signal delay_pointer_array_r      : delay_pointer_array_t := (others => (others => '0'));

  signal input_triggers_delayed   : std_logic_vector(7 downto 0);
  -- FIFO Parameters
  -- To delay the control signal by specified amount of time
  --signal control_signal_holder : std_logic_vector(1023 downto 0) := (others =>'0');
  signal counter_fifo : Unsigned (1023 downto 0) := (others => '0');

  signal signal_high                 : std_logic := '1';
  signal signal_low                 : std_logic := '0'; 
begin

  -- 1 = input; 0 = output
  nOED  <= '1';
  nOEE  <= '1';
  nOEF  <= '0';  
  nOEG  <= '1';

  -- NIM = 0; TTL = 1
  SELD  <= '1';
  SELE  <= '1';
  SELF  <= '1'; 
  SELG  <= '1';

  reset  <= not(nLBRES);

  nINT  <= '1'; -- interrupts not used
  
  --read registers
  mon_regs(0) <= std_logic_vector(to_unsigned(FWREV,32));
 	
  start_select <= ctrl_regs(0)(1);
  rate         <= ctrl_regs(1); 

  --clock stuff
  common_start <= counterL(To_Integer(Unsigned(rate)));
   
  clk_usr <= GIN(0) when ctrl_regs(15)(0)='1' else clk;   --Address: 0x183C Control of the clock: 0-Internal clock, 1-External clock from GIN(0)

  flipflop_inst1 : entity work.d_flipflop port map(
        d    => GIN(1),
        reset   => reset,
        clk   => pll_clk,
        q => flipflopsignal);

  flipflop_inst2 : entity work.d_flipflop port map(
         d    => flipflopsignal,
         reset   => reset,
         clk   => pll_clk,
         q => flipflopout); --F port 4

  --F(13) <= or_reduce(input_triggers_delayed);
  --F(13) <= input_triggers_delayed(0);
  --F(29) <= combined_enabled;



  pll_inst : entity pll.pll
    port map (
      refclk => clk_usr,
      rst => '0',
      outclk_0 => pll_clk,
      locked => open
 --     locked => LED(2)
    );

  Reference:process(pll_clk)
  begin 
    if rising_edge(pll_clk) then
      counterL <= counterL + 1; 
    end if;  

  end process;


  --- NEW FIRMWARE ---

  ----------TRIGGER SECTION -------------
  -- sps signals
  sps_signals(0) <= D(2);  --port 0  --EE
  sps_signals(1) <= D(18); --port 1  --WE
  sps_signals(2) <= D(3);  --port 2  --WWE


  -- busy signals
  busy_flags(3) <= D(19); --port 3 
  busy_flags(4) <= D(14); --port 4
  busy_flags(5) <= D(30); --port 5
  busy_flags(6) <= D(15); --port 6
  busy_flags(7) <= D(31); --port 7

  -- inputs signals 
  inputs(0) <= E(2);  --port 0
  inputs(1) <= E(18); --port 1
  inputs(2) <= E(3);  --port 2
  inputs(3) <= E(19); --port 3
  inputs(4) <= E(14); --port 4
  inputs(5) <= E(30); --port 5
  inputs(6) <= E(15); --port 6
  inputs(7) <= E(31); --port 7

  -- final signals - note that F is an output and uses different F numbers
  
  --F(0)  <= final_signal_delayed(0); --port 0
  --F(16) <= final_signal_delayed(1); --port 1
  --F(1)  <= final_signal_delayed(2); --port 2
  --F(17) <= final_signal_delayed(3); --port 3 
  --F(12) <= final_signal_delayed(4); --port 4
  --F(28) <= final_signal_delayed(5); --port 5
  --F(13) <= final_signal_delayed(6); --port 6
  --F(29) <= final_signal_delayed(7); --port 7
  -- TEMP
  -- F(28) <= pll_clk; --port 5
  --F(13) <= sps_busy_flags; --port 6
  --F(29) <= or_reduce(busy_flags_delayed); --port 7

  -- Laser
  laser_enable          <= ctrl_regs(13)(0);

  --Self_trigger_info
  st_enable             <= ctrl_regs(9)(0);                                 --Address: 0x1824 --    Bit 0: Enable/disable generation of self triggers
  st_downtime_vec       <= ctrl_regs(10);                                   --Address: 0x1828
  st_uptime_vec         <= ctrl_regs(11);                                   --Address: 0x182C
  st_downtime           <= TO_INTEGER(unsigned(st_downtime_vec));
  st_uptime             <= TO_INTEGER(unsigned(st_uptime_vec));

  sps_sim_enable        <= ctrl_regs(12)(0);                           --Address: 0x1830 -- Enable/disable auto generation of SPS signals
  
--  internal_busy_vec     <= ctrl_regs(13)(15 downto 0);                 --Address: 0x1834 -- Length of the internal busy signal
--  internal_busy_length  <= TO_INTEGER(unsigned(internal_busy_vec));  

  --control register information
--  flipped_input              <= ctrl_regs(2)(31 downto 24);  --Address: 0x180B
--  control_signal_selection   <= ctrl_regs(2)(11 downto 8);   --Address: 0x1809
--  enable_selection           <= ctrl_regs(2)(7 downto 0);    --Address: 0x1808
--  full_delay_time            <= ctrl_regs(3)(31 downto 0);   --Address: 0x180C
--  enabled_busy_flags         <= ctrl_regs(4)(7 downto 0);    --Address: 0x1810
--  sps_busy_flags             <= ctrl_regs(6)(0);             --Address: 0x1818  -- => 1 if we are in EE
--																										  -- => 0 if we are in WE or WEE
--																										  -- normally written by the run control
	flipped_input              <= ctrl_regs(2)(7 downto 0);    --Address: 0x1808
	strobe_signal              <= ctrl_regs(3)(0);    --Address: 0x180C
	enable_selection           <= ctrl_regs(4)(7 downto 0);    --Address: 0x1810
--      ctrl_regs(5)(2 downto 0);	  --Address: 0x1814  -- Used by the firmware to write SPS last signal
--	full_delay_time            <= ctrl_regs(6)(31 downto 0);   --Address: 0x1818
	enabled_busy_flags         <= ctrl_regs(7)(7 downto 0);    --Address: 0x181C
	sps_busy_flags             <= ctrl_regs(8)(0);             --Address: 0x1820  -- => 1 if we are in EE
                                                                                      -- => 0 if we are in WE or WEE
										      -- normally written by the run control
	internal_busy_reset        <= ctrl_regs(14)(0);            --Address: 0x1838

  -- Assigning signal delays defined by user in control registers to a signal holder
  -- I don't find a smarter way that single assignement
  -- Inputs
  delay_pointer_array_r(0)        <= ctrl_regs(16)(31 downto 0); --Address: 0x1840
  delay_pointer_array_r(1)        <= ctrl_regs(17)(31 downto 0); --Address: 0x1844
  delay_pointer_array_r(2)        <= ctrl_regs(18)(31 downto 0); --Address: 0x1848
  delay_pointer_array_r(3)        <= ctrl_regs(19)(31 downto 0); --Address: 0x184C
  delay_pointer_array_r(4)        <= ctrl_regs(20)(31 downto 0); --Address: 0x1850
  delay_pointer_array_r(5)        <= ctrl_regs(21)(31 downto 0); --Address: 0x1854
  delay_pointer_array_r(6)        <= ctrl_regs(22)(31 downto 0); --Address: 0x1858
  delay_pointer_array_r(7)        <= ctrl_regs(23)(31 downto 0); --Address: 0x185C
  -- Outputs
  delay_pointer_array_r(8)        <= ctrl_regs(24)(31 downto 0); --Address: 0x1860
  delay_pointer_array_r(9)        <= ctrl_regs(25)(31 downto 0); --Address: 0x1864
  delay_pointer_array_r(10)        <= ctrl_regs(26)(31 downto 0); --Address: 0x1868
  delay_pointer_array_r(11)        <= ctrl_regs(27)(31 downto 0); --Address: 0x186C
  delay_pointer_array_r(12)        <= ctrl_regs(28)(31 downto 0); --Address: 0x1870
  delay_pointer_array_r(13)        <= ctrl_regs(29)(31 downto 0); --Address: 0x1874
  delay_pointer_array_r(14)        <= ctrl_regs(30)(31 downto 0); --Address: 0x1878
  delay_pointer_array_r(15)        <= ctrl_regs(31)(31 downto 0); --Address: 0x187C

  --F(0)  <= shift_reg_array_r(8)(To_integer(Unsigned(delay_pointer_array_r(8)))); --port 0
  F(16) <= shift_reg_array_r(9)(To_integer(Unsigned(delay_pointer_array_r(9)))); --port 1
  F(1)  <= shift_reg_array_r(10)(To_integer(Unsigned(delay_pointer_array_r(10)))); --port 2
  F(17) <= shift_reg_array_r(11)(To_integer(Unsigned(delay_pointer_array_r(11)))); --port 3 
  F(12) <= shift_reg_array_r(12)(To_integer(Unsigned(delay_pointer_array_r(12)))); --port 4
  --F(28) <= shift_reg_array_r(13)(To_integer(Unsigned(delay_pointer_array_r(13)))); --port 5
  F(28) <= pll_clk;
  input_triggers_delayed(0) <= shift_reg_array_r(0)(To_integer(Unsigned(delay_pointer_array_r(0))));
  input_triggers_delayed(1) <= shift_reg_array_r(1)(To_integer(Unsigned(delay_pointer_array_r(1))));
  input_triggers_delayed(2) <= shift_reg_array_r(2)(To_integer(Unsigned(delay_pointer_array_r(2))));
  input_triggers_delayed(3) <= shift_reg_array_r(3)(To_integer(Unsigned(delay_pointer_array_r(3))));
  input_triggers_delayed(4) <= shift_reg_array_r(4)(To_integer(Unsigned(delay_pointer_array_r(4))));
  input_triggers_delayed(5) <= shift_reg_array_r(5)(To_integer(Unsigned(delay_pointer_array_r(5))));
  input_triggers_delayed(6) <= shift_reg_array_r(6)(To_integer(Unsigned(delay_pointer_array_r(6))));
  input_triggers_delayed(7) <= shift_reg_array_r(7)(To_integer(Unsigned(delay_pointer_array_r(7))));

  --input control signal only if its enabled
  --control_signal <= '1' when control_signal_selection = (control_signal_selection'range => '0')
  --                  else inputs(To_integer(Unsigned(control_signal_selection)) - 1);
  
  --send signals to the gdg
  gd_start(7 downto 0)   <= flipped_input xor inputs;
  gd_start(15 downto 8)  <= busy_flags(7 downto 0);
  gd_start(27 downto 18) <= (others => final_signal);
  
  --retrieve signals from the gdg
  --enabled_signals will always be '1' if they were not selected as enabled input
  -- enabled_signals      				<= gd_delayed(7 downto 0) or (enable_selection);
  enabled_signals      				<= input_triggers_delayed(7 downto 0) or (enable_selection);
  --busy_flags_delayed(7 downto 0)	<= gd_delayed(15 downto 8) and enabled_busy_flags(7 downto 0);
  busy_flags_delayed(7 downto 0)      <= busy_flags(7 downto 0) and enabled_busy_flags(7 downto 0);

  final_signal_delayed 				<= gd_delayed(27 downto 18);
  
  --the original control signal, delayed by specified amount of time
  --control_delayed <= control_signal_holder(to_integer(unsigned(full_delay_time)));
  
  --combine enabled signals and busy flags
  combined_enabled <= and_reduce(enabled_signals);
  --combined_busy <= (sps_busy_flags or or_reduce(combined_busy_vector) or internal_busy);
  combined_busy <= (sps_busy_flags or or_reduce(busy_flags_delayed) or internal_busy);
  
  -- Determine the Final Output Signal
--  final_signal <= '0' when enable_selection = (enable_selection'range => '0') else
--                  (control_delayed and combined_enabled and (not combined_busy));
  
  -- Send final signal to output
  --GOUT(1)<= combined_busy;
  --GOUT(0)<= internal_busy;

  --GOUT(0)<= combined_enabled;
  
  LED(0) <= st_enable;
  --LED(1) <= combined_busy;
  LED(1) <= or_reduce(busy_flags_delayed);--or_reduce(input_triggers_delayed);
  LED(2) <= sps_busy_flags;
  LED(3) <= internal_busy;
  LED(7) <= sps_sim_enable;
--  LED(4) <= ctrl_regs(5)(0);
--  LED(5) <= ctrl_regs(5)(1);
--  LED(6) <= ctrl_regs(5)(2);
--  LED(7) <= control_signal_selection;
	  
  
  -- Delay the Control Signal by the specified amount of delay time
  fifo_control_signal:process(pll_clk)
  begin
    if rising_edge(pll_clk) then 
      -- push vector back by one space and place '0' at the beginning
      --control_signal_holder    <= control_signal_holder(1022 downto 0) & '0';
		
      --place control signal value at the beginning
      --control_signal_holder(0) <= control_signal;
      
      shift_reg_array_r(0) <= shift_reg_array_r(0)(1022 downto 0) & '0';
      shift_reg_array_r(1) <= shift_reg_array_r(1)(1022 downto 0) & '0';
      shift_reg_array_r(2) <= shift_reg_array_r(2)(1022 downto 0) & '0';
      shift_reg_array_r(3) <= shift_reg_array_r(3)(1022 downto 0) & '0';
      shift_reg_array_r(4) <= shift_reg_array_r(4)(1022 downto 0) & '0';
      shift_reg_array_r(5) <= shift_reg_array_r(5)(1022 downto 0) & '0';
      shift_reg_array_r(6) <= shift_reg_array_r(6)(1022 downto 0) & '0';
      shift_reg_array_r(7) <= shift_reg_array_r(7)(1022 downto 0) & '0';
      shift_reg_array_r(8) <= shift_reg_array_r(8)(1022 downto 0) & '0';
      shift_reg_array_r(9) <= shift_reg_array_r(9)(1022 downto 0) & '0';
      shift_reg_array_r(10) <= shift_reg_array_r(10)(1022 downto 0) & '0';
      shift_reg_array_r(11) <= shift_reg_array_r(11)(1022 downto 0) & '0';
      shift_reg_array_r(12) <= shift_reg_array_r(12)(1022 downto 0) & '0';
      shift_reg_array_r(13) <= shift_reg_array_r(13)(1022 downto 0) & '0';
      shift_reg_array_r(14) <= shift_reg_array_r(14)(1022 downto 0) & '0';
      shift_reg_array_r(15) <= shift_reg_array_r(15)(1022 downto 0) & '0';

      -- Inputs
      shift_reg_array_r(0)(0) <= flipped_input(0) xor inputs(0); 
      shift_reg_array_r(1)(0) <= flipped_input(1) xor inputs(1);
      shift_reg_array_r(2)(0) <= flipped_input(2) xor inputs(2);
      shift_reg_array_r(3)(0) <= flipped_input(3) xor inputs(3);
      shift_reg_array_r(4)(0) <= flipped_input(4) xor inputs(4);
      shift_reg_array_r(5)(0) <= flipped_input(5) xor inputs(5);
      shift_reg_array_r(6)(0) <= flipped_input(6) xor inputs(6);
      shift_reg_array_r(7)(0) <= flipped_input(7) xor inputs(7);
      -- outputs
      shift_reg_array_r(8)(0) <= final_signal;
      shift_reg_array_r(9)(0) <= final_signal;
      shift_reg_array_r(10)(0) <= final_signal;
      shift_reg_array_r(11)(0) <= final_signal;
      shift_reg_array_r(12)(0) <= final_signal;
      shift_reg_array_r(13)(0) <= final_signal;
      shift_reg_array_r(14)(0) <= final_signal;
      shift_reg_array_r(15)(0) <= final_signal;
    -- force vhdl to save the value
    --else control_signal_holder <= control_signal_holder;
    end if;
  end process;
  
  
  -- Process to Write the SPS signals to a register to be read by software
  -- Read from Address: 0x1814
--  sps_control_signal:process(sps_signals(0),sps_signals(1),sps_signals(2))
--  begin
--    if rising_edge(sps_signals(0)) then 					-- WWE arrived
--		ctrl_regs(5)(31 downto 0) <= (others => '0');
--		ctrl_regs(5)(0) <= '1';
--	 elsif rising_edge(sps_signals(1)) then 				-- WE arrived
--	   ctrl_regs(5)(31 downto 0) <= (others => '0');
--		ctrl_regs(5)(1) <= '1';
--	 elsif rising_edge(sps_signals(2)) then				-- EE arrived
--	   ctrl_regs(5)(31 downto 0) <= (others => '0');
--		ctrl_regs(5)(2) <= '1';
--	 else ctrl_regs(5)(31 downto 0) <= ctrl_regs(5)(31 downto 0);
--  end if;
--  end process;
  --Trigger Process With Option Of Self Trig
  
  laser_processing:process(pll_clk)
  begin
    if laser_enable = '1' then
      F(0) <= shift_reg_array_r(8)(To_integer(Unsigned(delay_pointer_array_r(8)))); --port 0 
    else F(0) <= '0';
    end if;
  end process;



  trigger_processing:process(pll_clk)
  begin
    if st_enable = '1' then      
      if rising_edge(pll_clk) then
        st_counter <= st_counter+1;
        if st_counter >= st_downtime then
          final_signal <= ('1' and (not combined_busy));
          if st_counter >= st_downtime + st_uptime then
            st_counter <= 0;
          end if;
        else final_signal <= '0';
        end if;
      end if;
    elsif enable_selection = (enable_selection'range => '0') then
      final_signal <= '0';
    --else final_signal <= (control_delayed and combined_enabled and (not combined_busy));           
    else final_signal <= (combined_enabled and (not combined_busy));
    end if;   
  end process;

  sps_control_signal:process(pll_clk)
  begin
    if rising_edge(pll_clk) then
      if sps_sim_enable = '0' then
        ee_edge_detect 	<= ee_edge_detect(0) & sps_signals(0);
        we_edge_detect 	<= we_edge_detect(0) & sps_signals(1);
        wwe_edge_detect <= wwe_edge_detect(0) & sps_signals(2);
        if wwe_edge_detect = "01" then						-- WWE arrived
          --ctrl_regs(5)(31 downto 0) <= (others => '0');
          --ctrl_regs(5)(0) <= '1';
          mon_regs(1)(31 downto 0) <= (others => '0');
          mon_regs(1)(0) <= '1';
          LED(4) <= '0';
          LED(5) <= '0';
          LED(6) <= '1';
        elsif we_edge_detect = "01" then						-- WE arrived
          --ctrl_regs(5)(31 downto 0) <= (others => '0');
          --ctrl_regs(5)(1) <= '1';
          mon_regs(1)(31 downto 0) <= (others => '0');
          mon_regs(1)(1) <= '1';
          LED(4) <= '1';
          LED(5) <= '0';
          LED(6) <= '0';
        elsif ee_edge_detect = "01" then						-- EE arrived
          --ctrl_regs(5)(31 downto 0) <= (others => '0');
          --ctrl_regs(5)(2) <= '1';
          mon_regs(1)(31 downto 0) <= (others => '0');
          mon_regs(1)(2) <= '1';
          LED(4) <= '0';
          LED(5) <= '1';
          LED(6) <= '0';
        else ctrl_regs(5)(31 downto 0) <= ctrl_regs(5)(31 downto 0);
        end if;
      else 
        sps_counter <= sps_counter + 1; --Counter 0 starts with WE
        if (sps_counter < 40000000) then    --WE arrived
          --ctrl_regs(5)(31 downto 0) <= (others => '0');
          --ctrl_regs(5)(1) <= '1';
          mon_regs(1)(31 downto 0) <= (others => '0');
          mon_regs(1)(1) <= '1';
          LED(4) <= '1';
          LED(5) <= '0';
          LED(6) <= '0';
        elsif (sps_counter < 360000000) then   --EE arrived
          --ctrl_regs(5)(31 downto 0) <= (others => '0');
          --ctrl_regs(5)(2) <= '1';
          mon_regs(1)(31 downto 0) <= (others => '0');
          mon_regs(1)(2) <= '1';
          LED(4) <= '0';
          LED(5) <= '1';
          LED(6) <= '0';
        elsif (sps_counter < 760000000) then  --WWE arrived
          --ctrl_regs(5)(31 downto 0) <= (others => '0');
          --ctrl_regs(5)(0) <= '1';
          mon_regs(1)(31 downto 0) <= (others => '0');
          mon_regs(1)(0) <= '1';
          LED(4) <= '0';
          LED(5) <= '0';
          LED(6) <= '1';
        else sps_counter <= 0;
        end if;
      end if;
    end if;
  end process;

  trigger_flag:process(pll_clk)
  begin
    if rising_edge(pll_clk) then
      trigger_edge_detect <= trigger_edge_detect(0) & final_signal;
      if trigger_edge_detect = "01" then
	if (trigger_flag_counter = 4294967295 ) then 
		trigger_flag_counter <= 0;
	else trigger_flag_counter <= trigger_flag_counter + 1;
	end if;
        mon_regs(2) <= std_logic_vector(to_unsigned(trigger_flag_counter,32));
      end if;
    end if;
  end process;

  strobe_proc:process(pll_clk)
  begin
    if rising_edge(pll_clk) then
      strobe_edge_detect <= strobe_edge_detect(0) & strobe_signal;
      if strobe_edge_detect = "01" then
        F(13) <= '1';
	F(29) <= '1';
      else
        F(13) <= '0';
	F(29) <= '0';
      end if;
    end if;
  end process;

--Simulate SPS if not connected
--  sps_simulation_signal:process(clk)
--  begin
--    if sps_sim_enable = '1' then
--      if rising_edge(clk) then
--        sps_counter <= sps_counter + 1; --Counter 0 starts with WE
--        if (sps_counter < 300000000) then    --WE arrived
          --ctrl_regs(5)(31 downto 0) <= (others => '0');
          --ctrl_regs(5)(1) <= '1';
--          mon_regs(1)(31 downto 0) <= (others => '0');
---          mon_regs(1)(1) <= '1';
--          LED(4) <= '1';
--          LED(5) <= '0';
--          LED(6) <= '0';
--        elsif (sps_counter < 1300000000) then   --EE arrived
          --ctrl_regs(5)(31 downto 0) <= (others => '0');
          --ctrl_regs(5)(2) <= '1';
--          mon_regs(1)(31 downto 0) <= (others => '0');
--          mon_regs(1)(2) <= '1';
--          LED(4) <= '0';
--          LED(5) <= '1';
--          LED(6) <= '0';
--        elsif (sps_counter < 1550000000) then  --WWE arrived
          --ctrl_regs(5)(31 downto 0) <= (others => '0');
          --ctrl_regs(5)(0) <= '1';
--          mon_regs(1)(31 downto 0) <= (others => '0');
--          mon_regs(1)(0) <= '1';
--          LED(4) <= '0';
--          LED(5) <= '0';
--          LED(6) <= '1';
--        else sps_counter <= 0;
--        end if;
--      end if;
--    end if;
--  end process;

--  internal_busy_signal:process(clk)
--  begin
--    if rising_edge(clk) then
--      if final_signal = '1' and not internal_busy = '1' then
--        internal_busy <= '1';
--	mon_regs(3)(0) <= '1'; 
--      end if;
--      if internal_busy = '1' then
--        busy_counter <= busy_counter + 1;
--        if busy_counter > internal_busy_length then  
--          internal_busy <= '0';
--          busy_counter <= 0;
--        end if;
--      end if;
--    end if;
--  end process;

  -- We rise the interanl busy signal if final signal is '1' and interal_busy is not already up
  -- Then we write a 1 in mon_regs so the RC can check the status
  reset_internal_busy_signal:process(pll_clk)
  begin
    if rising_edge(pll_clk) then
      if final_signal = '1' and not internal_busy = '1' then
        internal_busy <= '1';
        mon_regs(3)(0) <= '1';
      else
        rst_busy_edge_detect <= rst_busy_edge_detect(0) & internal_busy_reset;
        if rst_busy_edge_detect = "10" then
	  internal_busy <= '0';
          mon_regs(3)(0) <= '0';
        end if;
      end if;
    end if;
  end process;



  ------------------------------------------------------------------------------------------------------------------
  ----------DATA READING SECTION -------------
  
  
  --Send registers A and B to specified output
  data_read:process(pll_clk)
  begin
	if rising_edge(pll_clk) then
	   data_a_reg(1 to 1499) <= data_a_reg(0 to 1498);
	   data_b_reg(1 to 1499)<= data_b_reg(0 to 1498); 
		
		data_a_reg(0) <= A;
		data_b_reg(0) <= B;
	end if;
  end process;
  
  

  -- ------------------------------------
  LB : entity work.LB_INT 
  -- ------------------------------------
    port map(
      -- Local Bus in/out signals
      reset      => reset,
      clk    => clk,
      nBLAST     => nBLAST,
      WnR        => WnR,
      nADS       => nADS,
      nREADY     => nREADY,
      LAD        => LAD,
      
      -- Internal Registers
      mon_regs    => mon_regs,       
      ctrl_regs => ctrl_regs,
      -- Gate and Delay controls
      gd_data_wr    => gd_data_wr,     
      gd_data_rd    => gd_data_rd,      
      gd_command    => gd_command,
      gd_write      => gd_write,
      gd_read       => gd_read,
      gd_ready      => gd_ready
    );
  
  -- ------------------------------------
  GD : entity  work.gd_control 
  -- ------------------------------------
    port map  (
      reset    =>  reset,
      -- ----------------------------------------------------------
      -- Clocks
      clk        =>  clk,                    -- Clock         
      
      -- Programming interface
      write       => gd_write,
      read        => gd_read, 
      writedata   => gd_data_wr,
      command     => gd_command,
      ready       => gd_ready,
      readdata    => gd_data_rd,    
      -- Gate&Delay control interface (SPI)        
      spi_sclk    => spi_sclk,    -- SPI clock
      spi_cs      => spi_cs,       -- SPI chip select
      spi_mosi    => spi_mosi,     -- SPI Master Out Slave In
      spi_miso    => spi_miso     -- SPI Master In Slave Out 
    );
  


end rtl;
