LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

entity d_flipflop is
    port (
        d : in std_logic;
        clk : in std_logic;
        reset : in std_logic;
        q : out std_logic
    );
end entity;

architecture behavior of d_flipflop is
    signal temp : std_logic;
begin
    process(clk, reset)
    begin
        if reset = '1' then
            temp <= '0';
        elsif rising_edge(clk) then
            temp <= d;
        end if;
    end process;

    q <= temp;
end behavior;
